# vagrant-ruby

A tiny Vagrant plugin that allows you to directly invoke Vagrant's
embedded Ruby interpreter (`ruby`) and associated programs (`gem`,
`rake`, etc.).

## Wait, Why?

Managing Ruby versions across diverse development environments is
notoriously painful, especially when non-Ruby devs are involved.

Meanwhile, lots of dev teams are already using Vagrant, so they already
have a perfectly good Ruby environment installed. To avoid the need for
`rvm`, `rbenv` et al., such a team can install this plugin and use
Vagrant's embedded Ruby instead.

## Installation

Fetch:

    bundle install

Build:

    rake gem

Install:

    vagrant plugin install --plugin-clean-sources --plugin-source "file://$(pwd)/repo/" vagrant-ruby

## Usage

The plugin adds five Vagrant commands:

    $ vagrant list-commands | grep "execute Vagrant's embedded"
    gem             execute Vagrant's embedded gem
    irb             execute Vagrant's embedded irb
    rake            execute Vagrant's embedded rake
    rdoc            execute Vagrant's embedded rdoc
    ruby            execute Vagrant's embedded ruby

The `ruby` command can be used in a script's hashbang, for example:

    #!/usr/bin/vagrant ruby
    puts "The currently-running Ruby is located at: #{RbConfig.ruby}"

The `ruby` command can also be used to execute a program from the
embedded Ruby's binary path with `ruby exec`, similarly to Bundler's
`bundle exec` command, for example:

    vagrant ruby exec rake [options ...]

## Contributing

The project lives on [GitLab][]. Clone, hack, and open a merge request.

[GitLab]: https://gitlab.com/catalyst-it/vagrant-ruby
